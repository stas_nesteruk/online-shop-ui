import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import hbs from 'hbs';
import { apiRouter } from '../../src/router';
import dateFormat from 'handlebars-dateformat';
import fetch from 'node-fetch';
import cookie from 'cookie';
const app = express();
const router = express.Router();

const publicDirectoryPath = path.join(process.cwd(), '/public');
const viewsDirectoryPath = path.join(process.cwd(), '/templates/views');
const partialsDirectoryPath = path.join(process.cwd(), '/templates/partials');

const MAIN_DOMAIN = process.env.MAIN_DOMAIN || 'http://localhost:4000';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

hbs.registerHelper('dateFormat', dateFormat);
app.set('view engine', 'hbs');
app.set('views', viewsDirectoryPath);


app.use((req, res, next) => {
    console.log("IT'S UI SIDE " + req.originalUrl);
    next();
});

const getCookie = async (url) => {
    const res = await fetch(url,  {
        credentials: 'include',
            headers: { 
                'Content-Type': 'application/json',
            }
    });
    return res;
};



app.use(async function(req, res, next) {
    const currentCookie = req.headers.cookie;
    let isSid = undefined;
    if(currentCookie){
        isSid = currentCookie.includes('sid');
    }
    if(!isSid){
        const response = await getCookie(MAIN_DOMAIN);
        const appCookie = response.headers.get('set-cookie');
        const cookies = cookie.parse(appCookie);
        res.cookie('sid', cookies.sid, {
            maxAge: 1000 * 60 * 60 * 24,
            httpOnly:true
        });
    }
    next();
});


hbs.registerPartials(partialsDirectoryPath);
hbs.registerHelper('ifCond', function (v1, operator, v2, options)  {
    switch (operator) {
        case '==': {
            console.log('v1' + v1);
            console.log('v2' + v2);
            return (v1 == v2) ? options.fn(this) : options.inverse(this);
        }
        case '===':
            return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '!=':
            return (v1 != v2) ? options.fn(this) : options.inverse(this);
        case '!==':
            return (v1 !== v2) ? options.fn(this) : options.inverse(this);
        case '<':
            return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
            return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
            return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
            return (v1 || v2) ? options.fn(this) : options.inverse(this);
        default:
            return options.inverse(this);
    }
});

app.use(express.static(publicDirectoryPath));

// router.get('*', (req, res) => {
//     res.render('error', {
//         error: 'Sorry, but our store unavailable now. Please come back later.'
//     });
// });

router.get('/', (req, res) => {
    res.redirect('/api/products');
});

router.get('*', (req, res) => {
    res.render('error', {
        error: 'Ups, something went wrong..'
    });
});

app.use(apiRouter);
app.use(router);

export default app;
