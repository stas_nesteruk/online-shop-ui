import { getCategoryList } from '../category';
import fetch from 'node-fetch';
import { getCurrentUser } from '../user/user.store';
//const MAIN_DOMAIN = 'https://nesteruk-shop-application.herokuapp.com';
const MAIN_DOMAIN = process.env.MAIN_DOMAIN || 'http://localhost:4000';

const sendPostData = async (url, { headers }) => {
    const res = await fetch(url, {
        method: 'POST',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
            Cookie: headers.cookie,
            Authorization: 'Bearer ' + getCurrentUser().token
        }
    });
    const body = await res.json();
    return body;
};

const getData = async (url, req) => {
    let headers = {};
    if(!getCurrentUser()){
        headers = {
            'Content-Type': 'application/json',
            Cookie: req.headers.cookie
        }
    }else{
        headers = {
            'Content-Type': 'application/json',
            Cookie: req.headers.cookie,
            Authorization: 'Bearer ' + getCurrentUser().token
        }
    }
    const res = await fetch(url, {
        method: 'GET',
        credentials: 'include',
        headers
    });
    const body = await res.json();
    return body;
};

const removeOrder = async (url, { headers }) => {
    const res = await fetch(url, {
        method: 'DELETE',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
            Cookie: headers.cookie,
            Authorization: 'Bearer ' + getCurrentUser().token
        }
    });
    const body = await res.json();
    return body;
};

export async function makeOrderTreatment(req, res) {
    try {
        const user = getCurrentUser();
        if (!user) {
            throw new Error('Please log in!');
        }
        const data = await sendPostData(MAIN_DOMAIN + req.originalUrl, req);
        const categories = await getCategoryList(req);
        if (!data.error) {
            return res.render('index', {
                partial: () => 'order',
                currentUser: getCurrentUser(),
                success: 'Made order successfully. Please wait for our manager to contact you soon. ',
                categories,
                products: data.products,
                totalCost: data.totalCost
            });
        } else {
            return res.render('index', {
                partial: () => 'order',
                currentUser: getCurrentUser(),
                failed: data.error,
                categories,
                products: data.products,
                totalCost: data.totalCost
            });
        }
    } catch (error) {
        res.render('error', {
            currentUser: getCurrentUser(),
            error: error.message
        });
    }
}

export async function getOrderTreatment(req, res) {
    try {
        const resCategories = await getCategoryList(req);
        const resOrder = await getData(MAIN_DOMAIN + req.originalUrl, req);
        if(!resOrder.success || !resCategories.success){
            return res.render('error', {
                currentUser: getCurrentUser(),
                error: resOrder.error.message ?  resOrder.error.message : 'Ups, something went wrong! Come back later!' 
            });
        }
        console.log(resOrder);
        res.render('index', {
            partial: () => 'order',
            categories: resCategories.categories,
            currentUser: getCurrentUser(),
            products: resOrder.order.products,
            totalCost: resOrder.order.totalCost
        });
    } catch (error) {
        res.render('error', {
            currentUser: getCurrentUser(),
            error: error.message
        });
    }
}

export async function getUserOrdersTreatment(req, res) {
    try {
        const resCategories = await getCategoryList(req);
        const resOrders = await getData(MAIN_DOMAIN + req.originalUrl, req);
        if(!resOrders.success || !resCategories.success){
            return res.render('error', {
                currentUser: getCurrentUser(),
                error: resOrders.error.message ?  resOrders.error.message : 'Ups, something went wrong! Come back later!' 
            });
        }
        let number = 1;
        resOrders.orders.forEach(order => {
            order.number = number++;
        });
        res.render('index', {
            partial: () => 'userOrders',
            currentUser: getCurrentUser(),
            categories: resCategories.categories,
            orders: resOrders.orders
        });
    } catch (error) {
        console.log(error);
        res.render('error', {
            currentUser: getCurrentUser(),
            error: error.message
        });
    }
}

export async function deleteOrderTreatment(req, res) {
    const id = req.params.id;
    try {
        const response = await removeOrder(MAIN_DOMAIN + req.originalUrl, req);
        if (!response.status === 200) {
            return res.status(400).send({
                error: response.error
            });
        }
        res.status(200).send({
            message: response.message
        });
    } catch (error) {
        res.render('error', {
            currentUser: getCurrentUser(),
            error: error.message
        });
    }
}
