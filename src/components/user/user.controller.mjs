import HTTP_STATUS from 'http-status';
import FormData from 'form-data';
import { postData, sendPostData, updateUserProfileInfo } from './user.service';
import { setCurrentUser, getCurrentUser } from './user.store';

const MAIN_DOMAIN = process.env.MAIN_DOMAIN || 'http://localhost:4000';

export async function showSignupPageTreatment(req, res) {
    res.render('signup');
}

export async function showLoginPageTreatment(req, res) {
    res.render('login', {
        login: 'admin@email.com',
        password: 'adminpass'
    });
}

export async function loginUserTreatment(req, res) {
    try {
        const response = await postData(
            MAIN_DOMAIN + req.originalUrl,
            req,
            req.body
        );
        if (!response.success) {
            return res.render('login', {
                error: response.error.message,
                login: req.body.email,
            });
        }
        let user = response.user;
        user.token = response.token;
        console.log(user);
        setCurrentUser(user);
        res.redirect('/api/products');
    } catch (error) {
        res.render('error', {
            currentUser: getCurrentUser(),
            error: error.message
        });
    }
}

export async function signupUserTreatment(req, res) {
    try {
        const { password, confirmPassword } = req.body;
        if (password !== confirmPassword) {
            return res.render('signup', {
                error: "Password isn't match!",
                name: req.body.name,
                email: req.body.email
            });
        }
        const response = await postData(
            MAIN_DOMAIN + req.originalUrl,
            req,
            req.body
        );
        if (!response.success) {
            return res.render('signup', {
                error: response.error.message,
                name: req.body.name,
                email: req.body.email
            });
        }
        setCurrentUser(response);
        res.redirect('/api/products');
    } catch (error) {
        res.render('error', {
            currentUser: getCurrentUser(),
            error: error.message
        });
    }
}

export async function logoutUserTreatment(req, res) {
    try {
        const response = await sendPostData(MAIN_DOMAIN + req.originalUrl, req);
        if (!response.success) {
            return res.render('error', {
                error: response.error.message
            });
        }
        setCurrentUser(undefined);
        res.redirect('/api/products');
    } catch (error) {
        res.render('error', {
            currentUser: getCurrentUser(),
            error: error.message
        });
    }
}

export async function logoutAllUserTreatment(req, res) {}

export function getProfileTreatment(req, res) {
    try {
        const user = getCurrentUser();
        if (user === undefined) {
            return res.redirect('/api/login');
        }
        const {
            name,
            email,
            phone = undefined,
            address = undefined,
            firstName = undefined,
            lastName = undefined,
            image
        } = user;
        res.render('userProfile', {
            currentUser: user,
            name: name,
            email,
            phone,
            address,
            firstName,
            lastName,
            image: MAIN_DOMAIN + image
        });
    } catch (error) {
        res.render('error', {
            currentUser: getCurrentUser(),
            error: error.message
        });
    }
}

export async function deleteUserTreatment(req, res) {}

export async function updateUserTreatment(req, res) {
    try {
        const data = req.body;
        let formData = new FormData();
        const keys = Object.keys(data);
        keys.forEach(key => {
            console.log(key + ' ' + data[key])
            formData.append(key, data[key]);
        });
        if (req.file) {
            formData.append('image', req.file.buffer, {
                fieldname: 'image',
                filename: req.file.originalname,
                contentType: 'image/jpeg',
            });
        }
        const response = await updateUserProfileInfo(MAIN_DOMAIN + req.originalUrl, req, formData);
        const user = response.user;
        user.token = getCurrentUser().token;
        setCurrentUser(user);
        const {
            name,
            email,
            phone = undefined,
            address = undefined,
            firstName = undefined,
            lastName = undefined,
            image
        } = user;
        res.status(HTTP_STATUS.OK).send({
            success: true,
            user: {
                name,
                email,
                phone,
                address,
                firstName,
                lastName,
                image: MAIN_DOMAIN + image
            }
        });
    } catch (error) {
        res.status(HTTP_STATUS.BAD_REQUEST).send({
            success: false,
            error: error.message
        });
    }
}
