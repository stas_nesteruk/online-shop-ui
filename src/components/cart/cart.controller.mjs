import HTTP_STATUS from 'http-status';
import { getCategoryList } from '../category';
import { getData } from '../product/product.service';
import fetch from 'node-fetch';
import { getCurrentUser } from '../user/user.store.mjs';
//const MAIN_DOMAIN = 'https://nesteruk-shop-application.herokuapp.com';
const MAIN_DOMAIN = process.env.MAIN_DOMAIN || 'http://localhost:4000';

export async function getCartTreatment(req, res) {
    try {
        const resCart = await getData(
            MAIN_DOMAIN + req.originalUrl,
            req
        );
        const resCategories = await getCategoryList(req);
        if(!resCart.success || !resCategories.success){
            return res.render('error', {
                currentUser: getCurrentUser(),
                error: 'Ups, something went wrong! Come back later!' 
            });
        }
        res.render('index', {
            partial: () => 'cart',
            categories: resCategories.categories,
            currentUser: getCurrentUser(),
            products: resCart.cart.products,
            totalCost: resCart.cart.totalCost
        });
    } catch (error) {
        res.render('error', {
            currentUser: getCurrentUser(),
            error: error.message
        });
    }
}

export async function getCartInfoTreatment(req, res) {
    try {
        console.log(MAIN_DOMAIN);
        fetch(MAIN_DOMAIN + '/api/shopping-cart', {
            method: 'get',
            headers: {
                'Content-Type': 'application/json',
                Cookie: req.headers.cookie
            }
        })
            .then(response => {
                return response.json();
            })
            .then(body => {
                res.status(HTTP_STATUS.OK).send(body);
            });
    } catch (error) {
        console.log(error);
    }
}

export async function addProductToCartTreatment(req, res) {
    try {
        fetch(MAIN_DOMAIN + req.originalUrl, {
            method: 'POST',
            body: JSON.stringify({
                id: req.body.id,
                count: req.body.count
            }),
            headers: {
                'Content-Type': 'application/json',
                Cookie: req.headers.cookie
            }
        })
            .then(response => {
                return response.json();
            })
            .then(body => {
                res.status(HTTP_STATUS.OK).send(body);
            });
    } catch (error) {
        res.status(HTTP_STATUS.INTERNAL_SERVER_ERROR).send(error);
    }
}

export async function deleteProductFromCartTreatment(req, res) {
    try {
        fetch(MAIN_DOMAIN + req.originalUrl, {
            method: 'PATCH',
            body: JSON.stringify({
                id: req.body.id,
                count: req.body.count
            }),
            headers: {
                'Content-Type': 'application/json',
                Cookie: req.headers.cookie
            }
        })
            .then(response => {
                return response.json();
            })
            .then(body => {
                console.log(body);
                res.status(HTTP_STATUS.OK).send(body);
            });
    } catch (error) {
        res.status(HTTP_STATUS.INTERNAL_SERVER_ERROR).send(error);
    }
}
