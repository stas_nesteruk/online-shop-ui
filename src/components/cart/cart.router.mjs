import express from 'express';

import {
    addProductToCartTreatment, 
    getCartTreatment,
    deleteProductFromCartTreatment,
    getCartInfoTreatment
} from './cart.controller'
const router = express.Router();

router.get('/shopping-cart', getCartTreatment);
router.get('/shopping-cart-info', getCartInfoTreatment);

router.post('/shopping-cart', addProductToCartTreatment);
router.patch('/shopping-cart', deleteProductFromCartTreatment);

export { router as cartRouter };