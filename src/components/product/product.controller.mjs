import HTTP_STATUS from 'http-status';
import { getCategoryList } from '../category';
import { getData } from './product.service';
import { getCurrentUser } from '../user/user.store';

//const MAIN_DOMAIN = 'https://nesteruk-shop-application.herokuapp.com';
const MAIN_DOMAIN = process.env.MAIN_DOMAIN || 'http://localhost:4000';

export async function getProductsTreatment(req, res) {
    try {
        const resProducts = await getData(MAIN_DOMAIN + req.originalUrl, req);
        const resCategories = await getCategoryList(req);
        if(!resProducts.success || !resCategories.success){
            return res.render('error', {
                currentUser: getCurrentUser(),
                error: 'Ups, something went wrong! Come back later!' 
            });
        }
        console.log(getCurrentUser());
        res.render('index', {
            partial: () => 'products',
            currentUser: getCurrentUser(),
            products: resProducts.products,
            categories: resCategories.categories, 
            currentCategory: req.originalUrl
        });
    } catch (error) {
        res.render('error', {
            currentUser: getCurrentUser(),
            error: error.message
        });
    }
}

export async function getProductsByCategoryTreatment(req, res) {
    try {
        const parts = req.originalUrl.split('/');
        const currentCategory = '/' + parts[parts.length-1];
        const resProducts = await getData(MAIN_DOMAIN + req.originalUrl, req);
        const resCategories = await getCategoryList(req);
        if(!resProducts.success || !resCategories.success){
            return res.render('error', {
                currentUser: getCurrentUser(),
                error: 'Ups, something went wrong! Come back later!' 
            });
        }
        res.render('index', {
            partial: () => 'products',
            currentUser: getCurrentUser(),
            products: resProducts.products,
            categories: resCategories.categories, 
            currentCategory
        });
    } catch (error) {
        res.render('error', {
            currentUser: getCurrentUser(),
            error: error.message
        });
    }
}

export async function getProductTreatment(req, res) {
    try {
        const resProduct = await getData(MAIN_DOMAIN + req.originalUrl, req);
        const resCategories = await getCategoryList(req);
        if(!resProduct.success || !resCategories.success){
            return res.render('error', {
                currentUser: getCurrentUser(),
                error: 'Ups, something went wrong! Come back later!' 
            });
        }
        res.render('index', {
            partial: () => 'productInfo',
            currentUser: getCurrentUser(),
            product: resProduct.product,
            categories: resCategories.categories
        });
    } catch (error) {
        res.render('error', {
            currentUser: getCurrentUser(),
            error: error.message
        });
    }
}

export async function searchProductsTreatment(req, res) {
    try {
        const searchQuery = req.query.value;
        const resProducts = await getData(MAIN_DOMAIN + req.originalUrl, req);
        const resCategories = await getCategoryList(req);
        if(!resProducts.success || !resCategories.success){
            return res.render('error', {
                currentUser: getCurrentUser(),
                error: 'Ups, something went wrong! Come back later!' 
            });
        }
        res.render('index', {
            partial: () => 'products',
            currentUser: getCurrentUser(),
            products: resProducts.products,
            categories: resCategories.categories, 
            searchQuery,
            foundNumber: resProducts.products.length    
        });
    } catch (error) {
        res.render('error', {
            currentUser: getCurrentUser(),
            error: error.message
        });
    }
}

export async function deleteProductByIdTreatment(req, res) {}


async function saveImage(file, category) {}

export async function createProductTreatment(req, res) {}

export async function updateProductTreatment(req, res) {}
