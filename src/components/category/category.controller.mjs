import HTTP_STATUS from 'http-status';
import { postNewCategory } from './category.service';

const MAIN_DOMAIN = process.env.MAIN_DOMAIN || 'http://localhost:4000';

export async function getCategoriesTreatment(req, res){

}

export async function deleteCategoryTreatment(req, res){
    
}

export async function updateCategoryTreatment(req, res){
    
}

export async function createCategoryTreatment(req, res){
    try{
        const category = req.body;
        console.log(category)
        const response = await postNewCategory(MAIN_DOMAIN + req.originalUrl, req, category);
        console.log(response);
        const body = await response.json();
        console.log(body);
        switch (response.status) {
            case HTTP_STATUS.UNAUTHORIZED:
                console.log('unauth')
                const body = await response.json();
                console.log(body);
                return res.status(HTTP_STATUS.UNAUTHORIZED).send(body);
            default:
                break;
        }
        res.status(HTTP_STATUS.CREATED).send(category);
    }catch(error){
        res.status(HTTP_STATUS.INTERNAL_SERVER_ERROR).send(error.message);
    }
}