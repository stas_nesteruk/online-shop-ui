import fetch from 'node-fetch';

export const postNewCategory = async (url, req, data) => {
    const res = await fetch(url, {
        method: 'Post',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            Cookie: req.headers.cookie
        }
    });
    return res;
}
