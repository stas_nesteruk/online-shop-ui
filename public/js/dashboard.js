$(function() {
    let init = () => {
        initAddCategoryBtn();
        $('#category-add-btn').click(addNewCategory);
    }

    let initAddCategoryBtn = function() {
        $('.category-add-btn').click(addNewCategory);
    };

    let addNewCategory = function() {
        const category = document.querySelector('#category-add .input-category').value;
        data = {
            category
        }
        console.log('starting');
        fetch('/api/category', {
            method: 'Post',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        })
        .then(res => {
            console.log(res)
            switch (res.status) {
                case 401:
                    break; 
                case 500:
                    showErrorMessage('category-add', res.statusText);
                    break;               
                default:
                    showSuccessMessage('category-add', 'Category ' + body.category + ' added!');
                break;
            }
        }).catch(error => {
            showErrorMessage('category-add', error.message);
        });
    }

    let showSuccessMessage = (idname, message) => {
        document.querySelector('#' + idname + ' .alert-success').style.display =
        'block';
        document.querySelector('#' + idname + ' .box-success-message').textContent =
        message;
    }

    let showErrorMessage = (idname, message) => {
        document.querySelector('#' + idname + ' .alert-danger').style.display =
        'block';
        document.querySelector('#' + idname + ' .box-error-message').textContent =
        message;
    }

init();

});